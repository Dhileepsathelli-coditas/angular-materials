import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonToggledComponent } from './button-toggled.component';

describe('ButtonToggledComponent', () => {
  let component: ButtonToggledComponent;
  let fixture: ComponentFixture<ButtonToggledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonToggledComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ButtonToggledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
